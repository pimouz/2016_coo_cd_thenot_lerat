package donnees;

/**
 * Created by RAPHAEL on 20/05/2016.
 */
public class SelecteurArtiste implements Selecteur {
   private String artiste;

    public SelecteurArtiste(String s){
        this.artiste  = s;
    }


    @Override
    public boolean select(CD cd) {
        return cd.getNomArtiste().equals(artiste);
    }
}
