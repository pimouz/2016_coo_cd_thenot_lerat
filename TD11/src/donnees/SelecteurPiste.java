package donnees;

/**
 * Created by RAPHAEL on 20/05/2016.
 */
public class SelecteurPiste implements Selecteur {
    private int nbpiste;

    public SelecteurPiste(int i){
        this.nbpiste = i;
    }


    @Override
    public boolean select(CD cd) {
        return cd.getPistes() == (nbpiste);
    }
}
