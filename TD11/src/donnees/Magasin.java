package donnees;

import java.util.ArrayList;

/**
 * La classe Magasin represente un magasin qui vend des CDs.</p>
 * 
 * cette classe est caracterisee par un ensemble de CDs correspondant aux CDS
 * vendus dans ce magasin.
 * 
 */
public class Magasin {

	/**
	 * la liste des CDs disponibles en magasin
	 */
	private ArrayList<CD> listeCds;

	/**
	 * construit un magasin par defaut qui ne contient pas de CD
	 */
	public Magasin() {
		listeCds = new ArrayList<CD>();
	}

	/**
	 * ajoute un cd au magasin
	 * 
	 * @param cdAAjouter
	 *            le cd a ajouter
	 */
	public void ajouteCd(CD cdAAjouter) {
		listeCds.add(cdAAjouter);
	}

	@Override
	/**
	 * affiche le contenu du magasin
	 */
	public String toString() {
		String chaineResultat = "";
		//parcours des Cds
		for (int i = 0; i < listeCds.size(); i++) {
			chaineResultat += listeCds.get(i);
		}
		chaineResultat += "nb Cds: " + listeCds.size();
		return (chaineResultat);

	}

	/**
	 * @return le nombre de Cds du magasin
	 */
	public int getNombreCds() {
		return listeCds.size();
	}
	
	/**
	 * permet d'acceder � un CD
	 * 
	 * @return le cd a l'indice i ou null si indice est non valide
	 */
	public CD getCd(int i)
	{
		CD res=null;
		if ((i>=0)&&(i<this.listeCds.size()))
			res=this.listeCds.get(i);
		return(res);
	}

	public void trierArtiste(){
		throw new Error();
	}

	public void trierAlbum(){
		for(int i = 0; i < this.listeCds.size() - 1; i++){
			CD cdsel = this.listeCds.get(i);
			int idsel = i;
			for(int j = i+1; j < this.listeCds.size(); j++){
				CD cdt = this.listeCds.get(j);
				int idt = j;
				if(cdt.etreAvantAlbum(cdsel)){
					cdsel = cdt;
					idsel = j;
				}
			}
			this.listeCds.set(i, cdsel);
			this.listeCds.set(idsel, this.listeCds.get(i));
		}
	}

	public ArrayList<CD> chercherArtiste(String nom){
		ArrayList<CD> newlist = new ArrayList<>();
		for(int i = 0; i < this.listeCds.size(); i++){
			if(listeCds.get(i).getNomArtiste().contains(nom)){
				newlist.add(listeCds.get(i));
			}
		}
		return newlist;
	}

	/*
	4.3 : changer le parametre : (Selecteur sel)
	if(sel.select(this.listCd.get(i)
		newList.add(this.listCd.get(i);
	 */

}
